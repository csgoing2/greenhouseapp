<?php
include "includes/sql_connect.php";

header('Content-Type: application/json');

$mysqli = $connection;

$chartquery = sprintf("SELECT datetime, temperature, humidity, pressure FROM weather_log where datetime >= now() - INTERVAL 7 DAY");

$result = $mysqli->query($chartquery);

$data = array();
foreach($result as $row)
{
  $data[] = $row;
}

$result->close();

print json_encode($data);
 ?>
