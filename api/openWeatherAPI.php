<?php  

$lat_lon_api_url = "http://api.openweathermap.org/geo/1.0/direct?q=Denver, CO, USA&appid=0cd4f809542f572be7d6cfb069ee4ed0";

$response = file_get_contents($lat_lon_api_url); //Read Json

$response_data = json_decode($response, true); // decode JSON data into PHP array

$lat = $response_data[0]['lat'];
$lon = $response_data[0]['lon'];

/* $forecast_url_p1 = "https://api.openweathermap.org/data/2.5/forecast?lat=";
$forecast_url_p2 = "&lon=";
$forecast_url_p3 = "&units=imperial&cnt=1&appid=0cd4f809542f572be7d6cfb069ee4ed0";
$forecast_url_complete = $forecast_url_p1 . $lat . $forecast_url_p2 . $lon . $forecast_url_p3;

$forecast_response = file_get_contents($forecast_url_complete);

$forecast_data = json_decode($forecast_response, true); */

$oneCall_url_p1 = "https://api.openweathermap.org/data/2.5/onecall?lat=";
$oneCall_url_p2 = "&lon=";
$oneCall_url_p3 = "&units=imperial&exclude=minutely,alerts&appid=0cd4f809542f572be7d6cfb069ee4ed0";
$oneCall_url_complete = $oneCall_url_p1 . $lat . $oneCall_url_p2 . $lon . $oneCall_url_p3;

$oneCall_response = file_get_contents($oneCall_url_complete);

$oneCall_data = json_decode($oneCall_response, true);

$utc_timezone = "UTC";
//$local_timezone = $oneCall_data['timezone']; //grab local timezone from oneCall API response
$local_utc_offset = $oneCall_data['timezone_offset'];

$current_datetime_utc = $oneCall_data['current']['dt'];
$current_datetime_converted = convertTimezone($current_datetime_utc, $local_utc_offset);
$sunrise_datetime_utc = $oneCall_data['current']['sunrise'];
$sunrise_datetime_converted = convertTimezone($sunrise_datetime_utc, $local_utc_offset);
$sunset_datetime_utc = $oneCall_data['current']['sunset'];
$sunset_datetime_converted = convertTimezone($sunset_datetime_utc, $local_utc_offset);

//print($current_datetime_utc . "\n");
echo (gmdate("Y-m-d\TH:i:s\Z", $current_datetime_converted));
echo " -- Current Datetime \n";
echo (gmdate("Y-m-d\TH:i:s\Z", $sunrise_datetime_converted));
echo " -- Sunrise Datetime \n";
echo (gmdate("Y-m-d\TH:i:s\Z", $sunset_datetime_converted));
echo " -- Sunset Datetime \n";

//print_r($oneCall_data);

function convertTimezone(int $datetime_to_convert, int $time_offset)
{
    return $datetime_to_convert + $time_offset;
}
?>