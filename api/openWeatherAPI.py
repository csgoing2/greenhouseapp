from urllib import request
import requests
import json

response = requests.get("http://api.openweathermap.org/geo/1.0/direct?q=Denver, CO, USA&appid=0cd4f809542f572be7d6cfb069ee4ed0")
print(response)

data = response.json() #store response json in data

lat = str(response.json()[0]['lat']) #pull latitude from XML
lon = str(response.json()[0]['lon'])  #pull longitude from XML

forecastp1 = "https://api.openweathermap.org/data/2.5/forecast?lat="
forecastp2 = "&lon="
forecastp3 = "&units=imperial&cnt=1&appid=0cd4f809542f572be7d6cfb069ee4ed0"
forecastRequest = forecastp1 + lat + forecastp2 + lon + forecastp3

response2 = requests.get(forecastRequest)

oneCallp1 = "https://api.openweathermap.org/data/2.5/onecall?lat="
oneCallp2 = "&lon="
oneCallp3 = "&units=imperial&cnt=&appid=0cd4f809542f572be7d6cfb069ee4ed0"
oneCallRequest = oneCallp1 + lat + oneCallp2 + lon + oneCallp3

response3 = requests.get(oneCallRequest)
print(response3.json())