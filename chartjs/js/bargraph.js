$(document).ready(function(){
  $.ajax({
    url : "http://localhost/greenhouseapp/api/weatherChartData.php",
    method : "GET",
    success : function(data){
      console.log(data);

      var datetime = [];
      var temperature = [];
      var humidity = [];
      var pressure = [];

      for(var i in data)
      {
        datetime.push(data[i].datetime);
        temperature.push(data[i].temperature);
        humidity.push(data[i].humidity);
        pressure.push(data[i].pressure);
      }

      var chartdata = {
        labels: datetime, temperature, humidity, pressure,
        datasets : [
          {
            label : 'Date and Time',
            backgroundColor: 'rgba(200,200,200, 0.75)',
            borderColor: 'rgba(200,200,200, 0.75)',
            hoverBackgroundColor: 'rgba(200,200,200, 1)',
            hoverBorderColor: 'rgba(200,200,200, 1)',
            data: datetime
          },
          {
            label : 'Temp',
            backgroundColor: 'rgba(200,200,200, 0.75)',
            borderColor: 'rgba(200,200,200, 0.75)',
            hoverBackgroundColor: 'rgba(200,200,200, 1)',
            hoverBorderColor: 'rgba(200,200,200, 1)',
            data: temperature
          }
        ],
      };

      var ctx = $("#mycanvasBar");

    var barGraph = new Chart(ctx, {
      type: 'bar',
      data: chartdata
    });
    },
    error : function(data){
        console.log(data);
    }
  });

});
