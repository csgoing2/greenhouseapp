$(document).ready(function(){
  $.ajax({
    url : "http://localhost/greenhouseapp/weatherChartData.php",
    type : "GET",
    success : function(data){
      console.log(data);

      var datetime = [];
      var temperature = [];
      var humidity = [];
      var pressure = [];

      for(var i in data)
      {
        datetime.push(data[i].datetime);
        temperature.push(data[i].temperature);
        humidity.push(data[i].humidity);
        pressure.push(data[i].pressure);
      }

      var chartdata = {
        labels: datetime, temperature,
        datasets : [
          {
            label : 'Date and Time',
            fill: false,
            lineTension: 0.1,
            backgroundColor: 'rgba(200,200,200, 0.75)',
            borderColor: 'rgba(200,200,200, 1)',
            pointHoverBackgroundColor: 'rgba(200,200,200, 1)',
            pointHoverBorderColor: 'rgba(200,200,200, 1)',
            data: datetime
          },
          {
            label : 'Temperature',
            fill: false,
            lineTension: 0.1,
            backgroundColor: 'rgba(200,50,50, 0.75)',
            borderColor: 'rgba(200,50,50, 1)',
            pointHoverBackgroundColor: 'rgba(200,50,50, 1)',
            pointHoverBorderColor: 'rgba(200,50,50, 1)',
            data: temperature
          },
          {
            label : 'Humidity',
            fill: false,
            lineTension: 0.1,
            backgroundColor: 'rgba(50,50,200, 0.75)',
            borderColor: 'rgba(50,50,200, 1)',
            pointHoverBackgroundColor: 'rgba(50,50,200, 1)',
            pointHoverBorderColor: 'rgba(50,50,200, 1)',
            data: humidity
          },
          {
            label : 'Barometric Pressure',
            fill: false,
            lineTension: 0.1,
            backgroundColor: 'rgba(50,200,50, 0.75)',
            borderColor: 'rgba(50,200,50, 1)',
            pointHoverBackgroundColor: 'rgba(50,200,50, 1)',
            pointHoverBorderColor: 'rgba(50,200,50, 1)',
            data: pressure
          }
        ]
      };

      var chartdata2 = {
        labels: datetime, temperature, humidity, pressure,
        datasets : [
          {
            label : 'Date and Time',
            fill: false,
            lineTension: 0.1,
            backgroundColor: 'rgba(200,200,200, 0.75)',
            borderColor: 'rgba(200,200,200, 1)',
            pointHoverBackgroundColor: 'rgba(200,200,200, 1)',
            pointHoverBorderColor: 'rgba(200,200,200, 1)',
            data: datetime
          },
          {
            label : 'Temperature',
            fill: false,
            lineTension: 0.1,
            backgroundColor: 'rgba(200,50,50, 0.75)',
            borderColor: 'rgba(200,50,50, 1)',
            pointHoverBackgroundColor: 'rgba(200,50,50, 1)',
            pointHoverBorderColor: 'rgba(200,50,50, 1)',
            data: temperature
          }
        ]
      };

      var ctx = $("#mycanvas");
      var ctx2 = $("#mycanvas2");

      var LineGraph = new Chart(ctx, {
        type: 'line',
        data: chartdata
      });

      var LineGraph2 = new Chart(ctx2, {
        type: 'line',
        data: chartdata2,
        options: {
        scales: {
          yAxes: [{
                ticks: {
                    suggestedMin: 0,
                    suggestedMax: 120
                }
            }]
        }
    }
      });

    },
    error : function(data){
      console.log(data);
    }
  });

});
