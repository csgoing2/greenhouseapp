$(document).ready(function(){
  $.ajax({
    url : "http://localhost/greenhouseapp/weatherChartData.php",
    type : "GET",
    async : true,
    success : function(data){
     console.log(data);

      var datetime = [];
      var temperature = [];
      var humidity = [];
      var pressure = [];

      for(var i in data)
      {
        datetime.push(data[i].datetime);
        temperature.push(data[i].temperature);
        humidity.push(data[i].humidity);
        pressure.push(data[i].pressure);
      }

      console.log(datetime);

      var lastUpdated = datetime[datetime.length-1];
      var mostRecentValues = [
        temperature[temperature.length-1],
        humidity[humidity.length-1],
        pressure[pressure.length-1]
      ];
      console.log(mostRecentValues);
      document.getElementById("lastUpdated").innerHTML = lastUpdated;
      document.getElementById("currentValHeader").innerHTML = mostRecentValues;


      var chartdata = {
        labels: datetime, temperature,
        datasets : [
          {
            label : 'Date and Time',
            fill: false,
            lineTension: 0.1,
            backgroundColor: 'rgba(200,200,200, 0.75)',
            borderColor: 'rgba(200,200,200, 1)',
            pointHoverBackgroundColor: 'rgba(200,200,200, 1)',
            pointHoverBorderColor: 'rgba(200,200,200, 1)',
            data: datetime
          },
          {
            label : 'Temperature',
            fill: false,
            lineTension: 0.1,
            backgroundColor: 'rgba(200,50,50, 0.75)',
            borderColor: 'rgba(200,50,50, 1)',
            pointHoverBackgroundColor: 'rgba(200,50,50, 1)',
            pointHoverBorderColor: 'rgba(200,50,50, 1)',
            data: temperature
          },
          {
            label : 'Humidity',
            fill: false,
            lineTension: 0.1,
            backgroundColor: 'rgba(50,50,200, 0.75)',
            borderColor: 'rgba(50,50,200, 1)',
            pointHoverBackgroundColor: 'rgba(50,50,200, 1)',
            pointHoverBorderColor: 'rgba(50,50,200, 1)',
            data: humidity
          },
          {
            label : 'Barometric Pressure',
            fill: false,
            lineTension: 0.1,
            backgroundColor: 'rgba(50,200,50, 0.75)',
            borderColor: 'rgba(50,200,50, 1)',
            pointHoverBackgroundColor: 'rgba(50,200,50, 1)',
            pointHoverBorderColor: 'rgba(50,200,50, 1)',
            data: pressure
          }
        ]
      };

      var ctx = $("#mycanvas3");
      var LineGraph = new Chart(ctx, {
        type: 'line',
        data: chartdata,
        options: {
        scales: {
          yAxes: [{
                ticks: {
                    suggestedMin: 0,
                    suggestedMax: 120
                }
            }]
        }
    }
      });

    },
    error : function(data){
      //console.log(data);
    }
  });

  $.ajax({
    url: "http://localhost/greenhouseapp/weekTrendData.php",
    type: "GET",
    async : true,
    success: function(data){
      //console.log(data);

      var datetimeForWeek = [];
      var temperatureForWeek = [];
      var humidityForWeek = [];
      var pressureForWeek = [];

      for(var i in data)
      {
        datetimeForWeek.push(data[i].datetime);
        temperatureForWeek.push(data[i].temperature);
        humidityForWeek.push(data[i].humidity);
        pressureForWeek.push(data[i].pressure);
      }

      var chartDataForWeek = {
        labels: datetimeForWeek, temperatureForWeek, humidityForWeek, pressureForWeek,
        datasets: [
          // {
          //   label : 'Date and Time',
          //   fill: false,
          //   lineTension: 0.1,
          //   backgroundColor: 'rgba(200,200,200, 0.75)',
          //   borderColor: 'rgba(200,200,200, 1)',
          //   pointHoverBackgroundColor: 'rgba(200,200,200, 1)',
          //   pointHoverBorderColor: 'rgba(200,200,200, 1)',
          //   data: datetimeForWeek
          // },
          {
            label : 'Temperature',
            fill: false,
            lineTension: 0.1,
            backgroundColor: 'rgba(200,50,50, 0.75)',
            borderColor: 'rgba(200,50,50, 1)',
            pointHoverBackgroundColor: 'rgba(200,50,50, 1)',
            pointHoverBorderColor: 'rgba(200,50,50, 1)',
            data: temperatureForWeek
          },
          {
            label : 'Humidity',
            fill: false,
            lineTension: 0.1,
            backgroundColor: 'rgba(50,50,200, 0.75)',
            borderColor: 'rgba(50,50,200, 1)',
            pointHoverBackgroundColor: 'rgba(50,50,200, 1)',
            pointHoverBorderColor: 'rgba(50,50,200, 1)',
            data: humidityForWeek
          },
          {
            label : 'Barometric Pressure',
            fill: false,
            lineTension: 0.1,
            backgroundColor: 'rgba(50,200,50, 0.75)',
            borderColor: 'rgba(50,200,50, 1)',
            pointHoverBackgroundColor: 'rgba(50,200,50, 1)',
            pointHoverBorderColor: 'rgba(50,200,50, 1)',
            data: pressureForWeek
          }]
      };

      var ctxWeek = $("#weekCanvas");

      var LineGraph = new Chart(ctxWeek, {
        type: 'line',
        data: chartDataForWeek,
        options: {
        scales: {
          yAxes: [{
                ticks: {
                    suggestedMin: 0,
                    suggestedMax: 120
                }
            }]
        }
    }
      });

    },
    error : function(data){
      //console.log(data);
    }
  });

  $.ajax({
    url: "http://localhost/greenhouseapp/monthTrendData.php",
    type: "GET",
    async : true,
    success: function(data){
      //console.log(data);

      var datetimeForMonth = [];
      var temperatureForMonth = [];
      var humidityForMonth = [];
      var pressureForMonth = [];

      for(var i in data)
      {
        datetimeForMonth.push(data[i].datetime);
        temperatureForMonth.push(data[i].temperature);
        humidityForMonth.push(data[i].humidity);
        pressureForMonth.push(data[i].pressure);
      }

      var chartDataForMonth = {
        labels: datetimeForMonth, temperatureForMonth, humidityForMonth, pressureForMonth,
        datasets: [
          // {
          //   label : 'Date and Time',
          //   fill: false,
          //   lineTension: 0.1,
          //   backgroundColor: 'rgba(200,200,200, 0.75)',
          //   borderColor: 'rgba(200,200,200, 1)',
          //   pointHoverBackgroundColor: 'rgba(200,200,200, 1)',
          //   pointHoverBorderColor: 'rgba(200,200,200, 1)',
          //   data: datetimeForMonth
          // },
          {
            label : 'Temperature',
            fill: false,
            lineTension: 0.1,
            backgroundColor: 'rgba(200,50,50, 0.75)',
            borderColor: 'rgba(200,50,50, 1)',
            pointHoverBackgroundColor: 'rgba(200,50,50, 1)',
            pointHoverBorderColor: 'rgba(200,50,50, 1)',
            data: temperatureForMonth
          },
          {
            label : 'Humidity',
            fill: false,
            lineTension: 0.1,
            backgroundColor: 'rgba(50,50,200, 0.75)',
            borderColor: 'rgba(50,50,200, 1)',
            pointHoverBackgroundColor: 'rgba(50,50,200, 1)',
            pointHoverBorderColor: 'rgba(50,50,200, 1)',
            data: humidityForMonth
          },
          {
            label : 'Barometric Pressure',
            fill: false,
            lineTension: 0.1,
            backgroundColor: 'rgba(50,200,50, 0.75)',
            borderColor: 'rgba(50,200,50, 1)',
            pointHoverBackgroundColor: 'rgba(50,200,50, 1)',
            pointHoverBorderColor: 'rgba(50,200,50, 1)',
            data: pressureForMonth
          }]
        };
        var ctxMonth = $("#monthCanvas");

        var LineGraph = new Chart(ctxMonth, {
          type: 'line',
          data: chartDataForMonth,
          options: {
          scales: {
            yAxes: [{
                  ticks: {
                      suggestedMin: 0,
                      suggestedMax: 120
                  }
              }]
          }
      }
        });

      },
      error : function(data){
        //console.log(data);
      }

    });
});
