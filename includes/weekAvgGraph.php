<tr>
                    <th class="chartName">Last Week Trend</th>
                </tr>
                <tr>
                    <td>
                    <div class="chart-container">
                        <canvas id="weekCanvas"></canvas>
                    </div>
                    </td>
                </tr>

<script type="text/javascript" src="chartjs/js/jquery.min.js"></script>
<script type="text/javascript" src="chartjs/js/Chart.min.js"></script>
<script type="text/javascript" src="chartjs/js/weatherApp.js"></script>