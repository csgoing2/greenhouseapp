<?php

include "includes/sql_connect.php";

$query = "SELECT * FROM weather_log";
$full_weather_query = mysqli_query($connection, $query);
?>

<!doctype html>

<html lang="en">
<head>
  <meta charset="utf-8">

  <title>Weather Monitoring</title>
  <meta name="description" content="Weather Montioring">
  <meta name="author" content="SitePoint">

  <link rel="stylesheet" href="css/styles.css?v=1.0">
  <style>
    tr:nth-child(even) {background: #CCC}
    tr:nth-child(odd) {background: #FFF}
  </style>
</head>

<body>
  <table>
    <th>Date</th>
    <th>Temperature</th>
    <th>Humidty</th>
    <th>Baro. Pressure</th>
  <?php
  while($row = mysqli_fetch_assoc($full_weather_query))
  {
    $date = $row['datetime'];
    $temp = $row['temperature'];
    $hum = $row['humidity'];
    $press = $row['pressure'];
#
    echo "<tr><td>{$date}</td><td>{$temp}</td><td>{$hum}</td><td>{$press}</td></tr>";
  }
   ?>
 </table>
</body>
</html>
