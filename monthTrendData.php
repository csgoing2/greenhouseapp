<?php
include "includes/sql_connect.php";

header('Content-Type: application/json');

$mysqli = $connection;

$chartquery = sprintf("SELECT day(datetime) as datetime, cast(avg(temperature) as decimal(10,2)) as temperature, cast(avg(humidity) as decimal(10,2)) as humidity, cast(avg(pressure) as decimal(10,2)) as pressure FROM weather_log group by month(datetime) order by month(datetime) ");

$result = $mysqli->query($chartquery);

$data = array();
foreach($result as $row)
{
  $data[] = $row;
}

$result->close();

print json_encode($data);
 ?>
