import Adafruit_DHT
import urllib.request
import urllib.parse
import time
import datetime
import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart

import ssl

try:
    _create_unverified_https_context = ssl._create_unverified_context
except AttributeError:
    # Legacy Python that doesn't verify HTTPS certificates by default
    pass
else:
    # Handle target environment that doesn't support HTTPS verification
    ssl._create_default_https_context = _create_unverified_https_context

email = "greenhousealert22@gmail.com"
pas = "sWa99AwsWa99Aw"

sms_gateway = '3522462803@txt.att.net'
# The server we use to send emails in our case it will be gmail but every email provider has a different smtp
# and port is also provided by the email provider.
smtp = "smtp.gmail.com"
port = 587
# This will start our email server
#server = smtplib.SMTP(smtp,port)


######################################################################
DHT_SENSOR = Adafruit_DHT.DHT22

DHT_PIN=4
smsDay = 0
smsHour = 0
while True:
    humidity, temperature = Adafruit_DHT.read_retry(DHT_SENSOR, DHT_PIN)

    if humidity is not None and temperature is not None:
        print("Temp={0:0.1f}*C  Humidity={1:0.1f}%".format(temperature, humidity))
    else:
        print("Failed to retrieve data from humidity sensor")

    url = 'https://192.168.1.2/greenhouseapp/add_data.php?temp='
    scheme = 'https'
    host = '192.168.1.2'
    path = 'greenhouseapp/add_data/php'
    queryParamHumidity = '&hum='
    queryParamPressure = '&press=0000/'
    convertedTemp = str(round(((temperature * 1.8) + 32), 3))
    convertedHumidity = str(round(humidity, 2))
    finalUrl = (url + convertedTemp + queryParamHumidity + convertedHumidity + queryParamPressure)
    print(finalUrl)
    url5 = urllib.parse.urlparse(finalUrl)
    print(url5)
    print(url5.geturl())
    
    urllib.request.urlopen(url5.geturl())

    now = datetime.datetime.now()
    today = now.day
    thisHour = now.hour

##########################################################################
    # if temperature < 4.5:
    #     print("temp is too low!")
    #     # Starting the server
    #     server.starttls()
    #     # Now we need to login
    #     server.login(email,pas)
    #
    #     # Now we use the MIME module to structure our message.
    #     msg = MIMEMultipart()
    #     msg['From'] = email
    #     msg['To'] = sms_gateway
    #     # Make sure you add a new line in the subject
    #     msg['Subject'] = "Temp!!\n"
    #     # Make sure you also add new lines to your body
    #     body = "Temperature is too low!\n"
    #     # and then attach that body furthermore you can also send html content.
    #     msg.attach(MIMEText(body, 'plain'))
    #
    #     sms = msg.as_string()
    #     server.sendmail(email,sms_gateway,sms)
    #     server.quit()
    #     smsTime = datetime.datetime.now()
    #     smsDay = smsTime.day
###########################################################################
    # print(today)
    # print(thisHour)
    time.sleep(900)
