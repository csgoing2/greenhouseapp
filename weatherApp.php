<!DOCTYPE html>
<html>
  <head>
      <title>V1 - GreenHouse Weather Tracker</title>
      <link rel="icon" href="images/favicon.png">
      <link rel="stylesheet" href="styles/styles.css">
  </head>
  <body>
    <h1 class='title'>Greenhouse Temp Tracker</h1>
    <div>
        <table class="main">
            <tr>
              <th>Last Updated</th>
            </tr>
            <tr>
              <td>
                <span id="lastUpdated">
                </span>
              </td>
            </tr>
            <tr>
              <th>Last Known Temp</th>
            </tr>
            <tr>
              <td>
                <span id="currentValHeader">
                </span>
              </td>
            </tr>
        <div>
          <tr>
            <th class="chartName">15min Interval Previous Days</th>
          </tr>
          <tr>
            <td>
                <div class="chart-container">
                <canvas id="mycanvas3"></canvas>
                </div>
          </td>
        </div>
        <div>
          <tr>
            <th class="chartName">Last Week Trend</th>
          </tr>
          <tr>
            <td>
              <div class="chart-container">
                <canvas id="weekCanvas"></canvas>
              </div>
            </td>
          </tr>
        </div>
        <div class="graph">
          <tr>
            <th class="chartName">Last Month Trend</th>
          </tr>
          <tr>
            <td>
              <div class="chart-container">
                <canvas id="monthCanvas"></canvas>
              </div>
            </td>
          </tr>
        </div>
        </table>
      </div>
      <script type="text/javascript" src="chartjs/js/jquery.min.js"></script>
      <script type="text/javascript" src="chartjs/js/Chart.min.js"></script>
      <script type="text/javascript" src="chartjs/js/weatherApp.js"></script>
  </body>
</html>
