<!DOCTYPE html>
<html>
  <head>
      <title>V1 - GreenHouse Weather Tracker</title>
      <link rel="icon" href="images/favicon.png">
      <link rel="stylesheet" href="styles/styles.css">
  </head>
  <body>
    <h1 class='titlev2'>Greenhouse Temp Tracker</h1>
    <div>
        <table class="main">
            <div> <!-- header -->
                <?php include 'includes/header.php' ?>
            </div>
            <div> <!-- 15 min interval bar chart -->
               <?php include 'includes/15minGraph.php' ?>
            </div>
            <div> <!-- Daily avg over week chart -->
                <?php include 'includes/weekAvgGraph.php' ?>
            </div>
            <div> <!-- Monthly average -->
                <?php include 'includes/monthAvgGraph.php' ?>
            </div>
        </table>
      </div>
  </body>
</html>
