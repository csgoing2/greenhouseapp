<?php
include "includes/sql_connect.php";

header('Content-Type: application/json');

$mysqli = $connection;

$chartquery = "SELECT datetime, temperature, humidity, pressure FROM weather_log  where datetime >= now() - INTERVAL 1 DAY and now() + INTERVAL 1 DAY";
//$chartquery = "SELECT datetime, temperature, humidity, pressure FROM weather_log where where datetime >= now() - INTERVAL 1 DAY and now() + INTERVAL 1 DAY";
$result = $mysqli->query($chartquery);
//$preppedquery = $mysqli->prepare($chartquery);


$data = array();  
/* $stmt = "SELECT * FROM weather_log";
$result = $connection->query($stmt);

if ($result->num_rows > 0) {
  // output data of each row
  while($row = $result->fetch_assoc()) {
    print_r($row);
  }
} else {
  echo "0 results";
}
$connection->close(); */

if ($result->num_rows > 0) {
  // output data of each row
  while($row = $result->fetch_assoc()) {
    array_push($data, $row);
  }
} else {
  echo "0 results";
}

$mysqli -> close();
$encodedJSON = json_encode($data);
print $encodedJSON;
 ?>
