<?php
include "includes/sql_connect.php";

header('Content-Type: application/json');

$mysqli = $connection;

$chartquery = sprintf("SELECT day(datetime) as datetime, cast(avg(temperature) as decimal(10,2)) as temperature, cast(avg(humidity) as decimal(10,2)) as humidity, cast(avg(pressure) as decimal(10,2)) as pressure FROM weather_log where datetime >= now() - INTERVAL 7 DAY and now() group by day(datetime) order by month(datetime), day(datetime) ");

$result = $mysqli->query($chartquery);

$data = array();
foreach($result as $row)
{
  $data[] = $row;
}

$result->close();

print json_encode($data);
 ?>
